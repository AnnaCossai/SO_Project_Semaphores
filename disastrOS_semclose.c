#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semClose(){

  int sem_fd = running->syscall_args[0];    //recupero file descriptor passato alla syscall

  SemDescriptor* sem_desc = SemDescriptorList_byFd(&running->sem_descriptors, sem_fd);    //recupero sem_desc tramite il suo fd
  if (!sem_desc){
    running->syscall_retvalue = DSOS_ESEMDESC;                 //errore descrittore, il semaforo non ha descrittore
    return;
  }

  SemDescriptorPtr* sem_desc_ptr = sem_desc->ptr;    //recupero il suo puntatore
  if (!sem_desc_ptr){
    running->syscall_retvalue = DSOS_ESEMDESCPTR;                 //errore puntatore descrittore, il descrittore non ha puntatore
    return;
  }

  Semaphore* semaphore = sem_desc->semaphore;   //recupero semaforo associato
  if (!semaphore){
    running->syscall_retvalue = DSOS_ENOTSEM;                 //errore semaforo = 0
    return;
  }

  sem_desc = (SemDescriptor*)List_detach(&running->sem_descriptors, (ListItem*)sem_desc);   //elimino sem_desc dalla lista del processo
  int result = SemDescriptor_free(sem_desc);   //libero memoria sem_desc
  if(result) {
    running->syscall_retvalue = DSOS_ESEMFREE;                  //errore nella free del descrittore
    return;
  }

  sem_desc_ptr = (SemDescriptorPtr*)List_detach(&semaphore->descriptors, (ListItem*)sem_desc_ptr);   //elimino sem_desc_ptr dalla lista dei descrittori
  if (semaphore->descriptors.size == 0 && semaphore->waiting_descriptors.size == 0) {
    semaphore = (Semaphore*)List_detach(&semaphores_list,(ListItem*)semaphore);   //elimino semaforo dalla lista globale dei semafori
    result = Semaphore_free(semaphore);    //libero memoria semaphore se finite le operazioni del semaforo
    if(result) {
      running->syscall_retvalue = DSOS_ESEMFREE;                  //errore nella free del semaforo
      return;
    }
  }

  result = SemDescriptorPtr_free(sem_desc_ptr);    //libero memoria sem_desc_ptr
  if(result) {
    running->syscall_retvalue = DSOS_ESEMFREE;                  //errore nella free del puntatore a descrittore
    return;
  }

  running->syscall_retvalue = 0;    //syscall ritorna 0 a operazione riuscita
  return;
}
