#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include "disastrOS.h"

#define BUFFER_SIZE 5
#define BUFFER_EMPTY  -1
#define SEM_FILL  101
#define SEM_EMPTY 102
#define SEM_PROD  103
#define SEM_CONS  104
#define SEM_GEN 105

int buf[BUFFER_SIZE];
int write_index=0;
int read_index=0;

// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Come on baby, don't fear the sleeper %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    //disastrOS_printStatus();
  }
}

void childProducer(void* args){
  printf("[P%d]Apro semafori:\n",disastrOS_getpid());
  int sem_prod = disastrOS_semopen(SEM_PROD, 1);
  int sem_fill = disastrOS_semopen(SEM_FILL, 0);
  int sem_empty = disastrOS_semopen(SEM_EMPTY, BUFFER_SIZE);
  disastrOS_printStatus();

  disastrOS_semwait(sem_empty);
  disastrOS_semwait(sem_prod);

  buf[write_index] = disastrOS_getpid();
  printf("[P%d]PRODUCO: %d\n",disastrOS_getpid(),buf[write_index]);
  write_index++;

  disastrOS_sempost(sem_prod);
  disastrOS_sempost(sem_fill);

  printf("[P%d]Chiudo semafori:\n",disastrOS_getpid());
  disastrOS_semclose(sem_empty);
  disastrOS_semclose(sem_fill);
  disastrOS_semclose(sem_prod);
  disastrOS_exit(disastrOS_getpid()+1);
}

void childConsumer(void* args){
  printf("[P%d]Apro semafori:\n",disastrOS_getpid());
  int sem_cons = disastrOS_semopen(SEM_CONS, 1);
  int sem_fill = disastrOS_semopen(SEM_FILL, 0);
  int sem_empty = disastrOS_semopen(SEM_EMPTY, BUFFER_SIZE);
  disastrOS_printStatus();

  disastrOS_semwait(sem_cons);
  disastrOS_semwait(sem_fill);

  printf("[P%d]CONSUMO: %d\n",disastrOS_getpid(),buf[read_index]);
  read_index++;

  disastrOS_sempost(sem_empty);
  disastrOS_sempost(sem_cons);

  printf("[P%d]Chiudo semafori:\n",disastrOS_getpid());
  disastrOS_semclose(sem_empty);
  disastrOS_semclose(sem_fill);
  disastrOS_semclose(sem_cons);
  disastrOS_exit(disastrOS_getpid()+1);
}

void initFunction(void* args){
  disastrOS_printStatus();
  printf("[P%d]Ciao, io sono la funzione init e comincio a lavorare.\n",disastrOS_getpid());
  disastrOS_spawn(sleeperFunction, 0);
  disastrOS_sleep(10);

  printf("[P%d]Apro i semafori.\n",disastrOS_getpid());
  int sem_prod = disastrOS_semopen(SEM_PROD, 1);
  int sem_cons = disastrOS_semopen(SEM_CONS, 1);
  int sem_fill = disastrOS_semopen(SEM_FILL, 0);
  int sem_empty = disastrOS_semopen(SEM_EMPTY, BUFFER_SIZE);
  disastrOS_printStatus();

  printf("\n[P%d]TEST #1\n",disastrOS_getpid());
  disastrOS_sleep(30);
  printf("[P%d]Farò il primo test creando 10 thread, alternando produttori e consumatori.\n",disastrOS_getpid());
  disastrOS_sleep(30);
  int alive_children=0;
  for(int i=0; i<BUFFER_SIZE; i++)
    buf[i]=BUFFER_EMPTY;
  for (int i=0; i<10; ++i) {
    if(i%2==0)
      disastrOS_spawn(childConsumer, 0);
    else
      disastrOS_spawn(childProducer, 0);
    alive_children++;
    disastrOS_sleep(10);
  }
  disastrOS_printStatus();
  int retval;
  int pid;
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){
    printf("[P%d]initFunction, child: %d terminata, retval:%d, alive: %d\n",disastrOS_getpid(),
     pid, retval, alive_children);
    --alive_children;
  }

  printf("\n--------------------------------------------------------------------------------------------------------------------------------\n");
  printf("\n[P%d]TEST #2\n",disastrOS_getpid());
  disastrOS_sleep(30);
  printf("[P%d]Farò il secondo test creando prima i produttori e poi i consumatori.\n",disastrOS_getpid());
  disastrOS_sleep(30);
  alive_children=0;
  for(int i=0; i<BUFFER_SIZE; i++)
    buf[i]=BUFFER_EMPTY;
  for (int i=0; i<4; ++i) {
    disastrOS_spawn(childProducer, 0);
    alive_children++;
    disastrOS_sleep(10);
  }
  for (int i=0; i<5; ++i) {
    disastrOS_spawn(childConsumer, 0);
    alive_children++;
    disastrOS_sleep(10);
  }
  disastrOS_spawn(childProducer, 0);
  alive_children++;
  disastrOS_sleep(10);
  disastrOS_printStatus();
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){
    printf("[P%d]initFunction, child: %d terminata, retval:%d, alive: %d\n",disastrOS_getpid(),
     pid, retval, alive_children);
    --alive_children;
  }

  printf("[P%d]Chiudo i semafori.\n",disastrOS_getpid());
  disastrOS_semclose(sem_empty);
  disastrOS_semclose(sem_fill);
  disastrOS_semclose(sem_cons);
  disastrOS_semclose(sem_prod);

  disastrOS_printStatus();
  printf("shutdown!\n");
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  printf("Il puntatore alla funzione del producer è: %p\n", childProducer);
  printf("Il puntatore alla funzione del consumer è: %p\n", childConsumer);
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
