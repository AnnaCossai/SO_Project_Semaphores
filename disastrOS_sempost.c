#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semPost(){

  int sem_fd = running->syscall_args[0];        //recupero parametro passato a disastrOS_sempost()
  SemDescriptor* sem_desc = SemDescriptorList_byFd(&running->sem_descriptors,sem_fd);   //prendo informazioni descrittore semaforo
  if (!sem_desc){
    running->syscall_retvalue = DSOS_ESEMDESC;                 //errore descrittore, il semaforo non ha descrittore
    return;
  }
  Semaphore* semaphore = sem_desc->semaphore;     //prendo informazioni del semaforo riferito al descrittore
  if (!semaphore){
    running->syscall_retvalue = DSOS_ENOTSEM;                 //errore semaforo = 0
    return;
  }

  semaphore->count++;             //incremento contatore semaforo

  if (semaphore->count <= 0) {
    SemDescriptorPtr* first_waiting_desc_ptr =
    (SemDescriptorPtr*) List_detach(&semaphore->waiting_descriptors, (ListItem*)(semaphore->waiting_descriptors).first);      //prendo primo descrittore che aspetta e lo rimuovo dalla lista dei waiting
    List_insert(&semaphore->descriptors, semaphore->descriptors.last, (ListItem*) first_waiting_desc_ptr);          //e lo inserisco in coda alla lista descrittori
    PCB* pcb =  first_waiting_desc_ptr->descriptor->pcb;                      //prendo pcb del primo descrittore in attesa
    List_detach(&waiting_list, (ListItem*) pcb);                              //lo rimuovo dalla lista di waiting
    List_insert(&ready_list, (ListItem*) ready_list.last, (ListItem*) pcb);    //lo inserisco alla coda di ready
    pcb->status = Ready;                //modifico lo stato del pcb da Waiting a Ready
  }

  running->syscall_retvalue = 0;        //se la system call va a buon fine restituisce 0
  return;
}
