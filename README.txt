Progetto di Sistemi Operativi sull'implementazione di semafori.

Sono state implementate le funzioni disastrOS_semopen, disastrOS_semwait,
disastrOS_sempost, disastrOS_semclose.
Modificati altri file per un corretto funzionamento, compreso il file di
test (disastrOS_test) per provare l'effettivo funzionamento dei semafori
su paradigma produttore-consumatore.

Sistemi Operativi 2017/2018 - 6 CFU
Domenico Muller 1708331
Anna Cossai 1702292
