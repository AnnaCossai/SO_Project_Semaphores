#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semWait(){
  int sem_fd = running->syscall_args[0];    //prendo il file descriptor passato come argomento alla syscall

  SemDescriptor* sem_desc = SemDescriptorList_byFd(&running->sem_descriptors, sem_fd);    //ricavo il descrittore del semaforo tramite il fd
  if (!sem_desc){
    running->syscall_retvalue = DSOS_ESEMDESC;                 //errore descrittore, il semaforo non ha descrittore
    return;
  }
  SemDescriptorPtr* sem_desc_ptr = sem_desc->ptr;   //ricavo il suo puntatore
  if (!sem_desc_ptr){
    running->syscall_retvalue = DSOS_ESEMDESCPTR;                 //errore puntatore descrittore, il descrittore non ha puntatore
    return;
  }
  Semaphore* semaphore = sem_desc->semaphore;   //ricavo la struttura del semaforo associato
  if (!semaphore){
    running->syscall_retvalue = DSOS_ENOTSEM;                 //errore semaforo = 0
    return;
  }

  semaphore->count--;   //decremento valore semaforo
  if(semaphore->count < 0){   //se minore di 0 vuol dire che c'è un altro processo in cs e quindi l'attuale processo deve andare in wait
    List_detach(&semaphore->descriptors, (ListItem*)sem_desc_ptr);    //rimuovo il descrittore dalla lista dei descrittori attivi
    List_insert(&semaphore->waiting_descriptors, semaphore->waiting_descriptors.last, (ListItem*)sem_desc_ptr);   //aggiungo il descrittore alla lista dei descrittori waiting
    running->status = Waiting;    //cambio lo stato del processo da Running a Waiting
    List_insert(&waiting_list, waiting_list.last, (ListItem*)running);    //inserisco il PCB nella lista dei processi waiting
    PCB* p = (PCB*)List_detach(&ready_list, (ListItem*)ready_list.first);   //ricavo il PCB del primo processo andato in Ready
    running = (PCB*)p;                                                      //e lo metto in running
  }

  running->syscall_retvalue = 0;
  return;
}
