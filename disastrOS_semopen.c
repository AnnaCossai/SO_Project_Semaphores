#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semOpen(){

  int sem_id = running->syscall_args[0];    //recupero parametri passati a disastrOS_semopen()
  int sem_count = running->syscall_args[1];

  Semaphore* semaphore = SemaphoreList_byId(&semaphores_list, sem_id);    //controllo se semaforo è in lista semafori
  if (!semaphore) {
    semaphore = Semaphore_alloc(sem_id, sem_count);                        //se non c'è, lo alloco
    if (!semaphore){
      running->syscall_retvalue = DSOS_ESEMALLOC;                 //errore allocazione se semaforo = 0
      return;
    }
    if (sem_count < 0){
      running->syscall_retvalue = DSOS_ESEMVALUE;                 //errore valore se semaforo inizializzato con valore negativo
      return;
    }
    List_insert (&semaphores_list, semaphores_list.last, (ListItem*) semaphore);    //aggiungo il semaforo in coda alla lista semafori
  }

  int sem_fd = running->last_sem_fd;            //prendo file descriptor da utilizzare
  running->last_sem_fd++;                       //incremento prossimo file descriptor

  SemDescriptor* sem_desc = SemDescriptor_alloc(sem_fd, semaphore, running);    //alloco descrittore relativo al semaforo
  if(!sem_desc){
    running->syscall_retvalue = DSOS_ESEMDESC;              //errore descrittore se = 0
    return;
  }
  List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*) sem_desc); //e lo inserisco in coda alla lista dei descrittori

  SemDescriptorPtr* sem_desc_ptr = SemDescriptorPtr_alloc(sem_desc);    //alloco puntatore descrittore
  if(!sem_desc_ptr){
    running->syscall_retvalue = DSOS_ESEMDESCPTR;              //errore puntatore descrittore se = 0
    return;
  }
  sem_desc->ptr = sem_desc_ptr;     //collego il descrittore al suo puntatore
  List_insert(&semaphore->descriptors, semaphore->descriptors.last, (ListItem*) sem_desc_ptr);  //inserisco puntatore descrittore alla propria lista

  running->syscall_retvalue = sem_desc->fd;       //la system call deve restituire il file descriptor
  return;

}
